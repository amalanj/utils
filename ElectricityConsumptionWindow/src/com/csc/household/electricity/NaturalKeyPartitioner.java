package com.csc.household.electricity;

import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Partitioner;

public class NaturalKeyPartitioner implements
		Partitioner<TimeSeriesKey, TimeSeriesDataPoint> {

	@Override
	public int getPartition(TimeSeriesKey key, TimeSeriesDataPoint value,
			int numPartitions) {
		return Math.abs(key.getConsumerId().hashCode() * 127) % numPartitions;
	}

	@Override
	public void configure(JobConf arg0) {
		// TODO Auto-generated method stub

	}
}