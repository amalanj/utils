package com.csc.household.electricity;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class ConsumptionWindowReducer extends MapReduceBase
		implements
			Reducer<TimeSeriesKey, TimeSeriesDataPoint, Text, Text> {

	static enum PointCounters {
		POINTS_SEEN, POINTS_ADDED_TO_WINDOWS, MOVING_AVERAGES_CALCD
	};
	
	private JobConf configuration;

	@Override
	public void configure(JobConf job) {

		this.configuration = job;

	}

	static long hour_in_ms = 60 * 60 * 1000;
	
	Text keyText = new Text();
	Text keyValue = new Text();

	public void reduce(TimeSeriesKey key, Iterator<TimeSeriesDataPoint> values,
			OutputCollector<Text, Text> output, Reporter reporter) throws IOException {

		/*
		TimeSeriesDataPoint next_point;
		float point_sum = 0;
		float moving_avg = 0;

		int iWindowSizeInHours = 24;
		int iWindowStepSizeInHours = 1;

		long iWindowSizeInMS = iWindowSizeInHours * hour_in_ms;
		long iWindowStepSizeInMS = iWindowStepSizeInHours * hour_in_ms;

		Text out_key = new Text();
		Text out_val = new Text();

		SlidingWindow sliding_window = new SlidingWindow(iWindowSizeInMS,
				iWindowStepSizeInMS, hour_in_ms);

		while (values.hasNext()) {

			while (sliding_window.WindowIsFull() == false && values.hasNext()) {

				reporter.incrCounter(PointCounters.POINTS_ADDED_TO_WINDOWS, 1);

				next_point = values.next();

				TimeSeriesDataPoint p_copy = new TimeSeriesDataPoint();
				p_copy.copy(next_point);

				try {
					sliding_window.AddPoint(p_copy);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (sliding_window.WindowIsFull()) {

				reporter.incrCounter(PointCounters.MOVING_AVERAGES_CALCD, 1);

				LinkedList<TimeSeriesDataPoint> oWindow = sliding_window
						.GetCurrentWindow();

				String strBackDate = oWindow.getLast().getDate();

				// ---------- compute the moving average here -----------

				out_key.set("Consumer: " + key.getConsumerId() + ", Date: " + strBackDate);

				point_sum = 0;

				for (int x = 0; x < oWindow.size(); x++) {

					point_sum += oWindow.get(x).fValue;

				} // for

				moving_avg = point_sum / oWindow.size();

				out_val.set("Average Consumption: " + moving_avg);

				output.collect(out_key, out_val);

				// 2. step window forward

				sliding_window.SlideWindowForward();

			}

		} // while

		out_key.set("debug > " + key.getConsumerId()
				+ " --------- end of group -------------");
		out_val.set("");

		output.collect(out_key, out_val);
		
		*/
		
		while (values.hasNext()) {
			
		}

	} // reduce

}
