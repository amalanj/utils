package com.csc.household.electricity;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class ConsumptionWindowMapper extends MapReduceBase implements Mapper<LongWritable, Text, TimeSeriesKey, TimeSeriesDataPoint> {

	private final TimeSeriesKey key = new TimeSeriesKey();
	private final TimeSeriesDataPoint val = new TimeSeriesDataPoint();

	
	@Override
	public void map(LongWritable inkey, Text value,
			OutputCollector<TimeSeriesKey, TimeSeriesDataPoint> output, Reporter reporter)
			throws IOException {
		
		String line = value.toString();
		ConsumptionDataPoint dataPoint = ConsumptionDataPoint.parse(line);
		if(dataPoint != null){
			key.set(dataPoint.consumerId, dataPoint.consumptionTime);
			
			val.fValue = dataPoint.subMeter1 + dataPoint.subMeter2 + dataPoint.subMeter3;
			// val.lDateTime = dataPoint.consumptionTime;
			val.lDateTime = dataPoint.consumptionTime / (6 * 60 * 60);
			
			output.collect(key, val);
		}
		
	}

}
