package com.csc.household.electricity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class TimeSeriesKey implements WritableComparable<TimeSeriesKey> {
	
	private String consumerId = "";
	private long ts = 0;

	public void set(String consumerId, long lTS) {
		this.consumerId = consumerId;
		this.ts = lTS;
	}
	
	public String getConsumerId() {
		return consumerId;
	}

	public long getTs() {
		return ts;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		
		this.consumerId = in.readUTF();
		this.ts = in.readLong();
		
	}

	@Override
	public void write(DataOutput out) throws IOException {
		
		out.writeUTF(this.consumerId);
		out.writeLong(this.ts);
		
	}

	@Override
	public int compareTo(TimeSeriesKey other) {
		
		if (this.consumerId.compareTo(other.consumerId) != 0) {
			return this.consumerId.compareTo(other.consumerId);
		} else if (this.ts != other.ts) {
			return ts < other.ts ? -1 : 1;
		} else {
			return 0;
		}
		
	}

}
