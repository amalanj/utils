package com.csc.household.electricity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ConsumptionDataPoint {
	
	public String consumerId;
	public long consumptionTime;
	public float subMeter1;
	public float subMeter2;
	public float subMeter3;
	
	private static final int TOTAL_FIELDS = 10;
	
	private static final String DATE_FORMAT = "dd/MM/yy hh:mm:ss";
	private static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
	
	public static ConsumptionDataPoint parse(String records){
		ConsumptionDataPoint parsedRec = new ConsumptionDataPoint();
		
		String[] values = records.split(",");
		if(values.length != TOTAL_FIELDS){
			return null;
		}
		parsedRec.consumerId = values[0];
		String consumedDate = values[1]+" "+values[2];
		try{
			parsedRec.consumptionTime = sdf.parse(consumedDate).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		parsedRec.subMeter1 = Float.parseFloat(values[7]);
		parsedRec.subMeter2 = Float.parseFloat(values[8]);
		parsedRec.subMeter3 = Float.parseFloat(values[9]);
		
		return parsedRec;
	}

}
