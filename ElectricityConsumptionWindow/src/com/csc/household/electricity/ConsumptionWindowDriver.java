package com.csc.household.electricity;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class ConsumptionWindowDriver extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ConsumptionWindowDriver(),
				args);
		System.exit(res);
	}

	@Override
	public int run(String[] arg0) throws Exception {
		JobConf conf = new JobConf(getConf(), ConsumptionWindowDriver.class);
		conf.setJobName("ElectricityConsumptionWindowJob");
		
		conf.setInputFormat(TextInputFormat.class);

		conf.setMapOutputKeyClass(TimeSeriesKey.class);
		conf.setMapOutputValueClass(TimeSeriesDataPoint.class);

		conf.setMapperClass(ConsumptionWindowMapper.class);
		conf.setPartitionerClass(NaturalKeyPartitioner.class);
		conf.setOutputKeyComparatorClass(CompositeKeyComparator.class);
		conf.setOutputValueGroupingComparator(NaturalKeyGroupingComparator.class);
		
		conf.setReducerClass(ConsumptionWindowReducer.class);

		conf.setOutputFormat(TextOutputFormat.class);
		conf.setCompressMapOutput(true);

		FileInputFormat.setInputPaths(conf, new Path(arg0[0]));
		FileOutputFormat.setOutputPath(conf, new Path(arg0[1]));

		JobClient.runJob(conf);

		return 0;
	}

}
