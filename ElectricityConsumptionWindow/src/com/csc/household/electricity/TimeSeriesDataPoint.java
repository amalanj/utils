package com.csc.household.electricity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.hadoop.io.Writable;

public class TimeSeriesDataPoint implements Writable {

	public long lDateTime;
	public float fValue;

	private static final String DATE_FORMAT = "dd/MM/yy hh:mm:ss";
	private static SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

	/**
	 * Deserializes the point from the underlying data.
	 * 
	 * @param in
	 *            A DataInput object to read the point from.
	 * @see java.io.DataInput
	 * @see org.apache.hadoop.io.Writable#readFields(java.io.DataInput)
	 * 
	 */
	public void readFields(DataInput in) throws IOException {

		this.lDateTime = in.readLong();
		this.fValue = in.readFloat();
	}

	/**
	 * This is a static method that deserializes a point from the underlying
	 * binary representation.
	 * 
	 * @param in
	 *            A DataInput object that represents the underlying stream to
	 *            read from.
	 * @return A TimeseriesDataPoint
	 * @throws IOException
	 */
	public static TimeSeriesDataPoint read(DataInput in) throws IOException {

		TimeSeriesDataPoint p = new TimeSeriesDataPoint();
		p.readFields(in);
		return p;

	}

	public String getDate() {

		return sdf.format(this.lDateTime);

	}

	public void copy(TimeSeriesDataPoint source) {

		this.lDateTime = source.lDateTime;
		this.fValue = source.fValue;

	}

	@Override
	public void write(DataOutput out) throws IOException {

		out.writeLong(this.lDateTime);
		out.writeFloat(this.fValue);

	}

}
