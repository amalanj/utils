#!/bin/sh

##
# Usage ./tableCount.sh dbname outFileName
# Eg: ./tableCount.sh stg stg_non_empty_tables.txt
##

dbname=$1
eval "hive -S -e 'use $dbname; show tables'" > $dbname
while read line
do
  result=$(eval "hive -S -e 'use $dbname; select * from $line limit 5'")
  if [ ! -z "$result" ]; then
    echo "$line"
  fi
done < "$dbname"
