package com.csc.household.electricity;

public class Consumption {
	private String consumptionDate;
	private String consumptionTime;
	private float consumptionActivePower;
	private float consumptionReactivePower;
	private float consumptionVoltage;
	private float consumptionIntensity;
	private float consumptionMeter1;
	private float consumptionMeter2;
	private float consumptionMeter3;
}
