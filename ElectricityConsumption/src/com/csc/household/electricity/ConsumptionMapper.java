package com.csc.household.electricity;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class ConsumptionMapper extends MapReduceBase
		implements
			Mapper<LongWritable, Text, Text, FloatWritable> {

	@Override
	public void map(LongWritable key, Text value,
			OutputCollector<Text, FloatWritable> output, Reporter reporter)
			throws IOException {
		Text word = new Text();
		Float totalConsumption = (float) 0;
		String[] consumptionRecord = value.toString().split(";");
		if (valid(consumptionRecord)) {
			word.set(consumptionRecord[0]);
			totalConsumption = Float.parseFloat(consumptionRecord[6])
					+ Float.parseFloat(consumptionRecord[7])
					+ Float.parseFloat(consumptionRecord[8]);
			output.collect(word, new FloatWritable(totalConsumption));
		}
	}

	private boolean valid(String[] consumptionRecord) {

		if (consumptionRecord.length == 9
				&& !consumptionRecord[0].equalsIgnoreCase("date")
				&& !consumptionRecord[6].equalsIgnoreCase("?")) {
			return true;
		}
		return false;

	}
}
