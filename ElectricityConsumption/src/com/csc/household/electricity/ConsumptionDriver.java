package com.csc.household.electricity;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;

public class ConsumptionDriver {

	public static void main(String[] args) {
		JobClient client = new JobClient();
		JobConf conf = new JobConf(
				com.csc.household.electricity.ConsumptionDriver.class);

		// TODO: specify output types
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(FloatWritable.class);

		// / TODO: specify input and output DIRECTORIES (not files)
		FileInputFormat.addInputPath(conf, new Path(args[0]));
		// set the HDFS path of the input data
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		// TODO: specify a mapper
		conf.setMapperClass(ConsumptionMapper.class);

		// TODO: specify a reducer
		conf.setReducerClass(ConsumptionReducer.class);

		client.setConf(conf);
		try {
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
