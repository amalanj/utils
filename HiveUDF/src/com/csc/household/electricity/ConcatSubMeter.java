package com.csc.household.electricity;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.DoubleObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

public class ConcatSubMeter extends GenericUDF {
	
	DoubleObjectInspector meter1;
	DoubleObjectInspector meter2;
	DoubleObjectInspector meter3;

	@Override
	public Object evaluate(DeferredObject[] arg0) throws HiveException {
		Double m1 = (Double) meter1.getPrimitiveJavaObject(arg0[0].get());
		Double m2 = (Double) meter1.getPrimitiveJavaObject(arg0[1].get());
		Double m3 = (Double) meter1.getPrimitiveJavaObject(arg0[2].get());
		return m1+m2+m3;
	}

	@Override
	public String getDisplayString(String[] arg0) {
		StringBuilder sb = new StringBuilder();
		sb.append("The arguments are ");
		sb.append(arg0[0]+", ");
		sb.append(arg0[1]+", ");
		sb.append(arg0[2]);
		return sb.toString();
	}

	@Override
	public ObjectInspector initialize(ObjectInspector[] arg)
			throws UDFArgumentException {
		// TODO Auto-generated method stub
		if (arg.length != 2) {
      throw new UDFArgumentLengthException("arrayContainsExample only takes 2 arguments: List<T>, T");
    }
		
		ObjectInspector a = arg[0];
    ObjectInspector b = arg[1];
    ObjectInspector c = arg[2];
    if (!(a instanceof DoubleObjectInspector) || !(b instanceof DoubleObjectInspector)  || !(c instanceof DoubleObjectInspector)) {
      throw new UDFArgumentException("arguments should be of type Double");
    }
    
		this.meter1 = (DoubleObjectInspector) arg[0];
		this.meter2 = (DoubleObjectInspector) arg[1];
		this.meter3 = (DoubleObjectInspector) arg[2];
		
		return PrimitiveObjectInspectorFactory.javaBooleanObjectInspector;
		
	}
	
	

}
