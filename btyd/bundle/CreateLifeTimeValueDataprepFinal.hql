USE ${hiveconf:hivedb};
CREATE EXTERNAL TABLE IF NOT EXISTS DataPrepFinal
(
CustomerKey BIGINT,
AcquisitionDate STRING,
CalibStartDate STRING,
CalibEndDate STRING,
MinPurchaseDate STRING,
MaxPurchaseDate STRING,
TransactionDays DOUBLE,
Spend DOUBLE,
RepeatTransactionDays DOUBLE,
RepeatSpend DOUBLE,
CustomerDuration DOUBLE,
StudyDuration DOUBLE,
TruncatedTransactionDays DOUBLE,
TruncatedAverageSpend DOUBLE,
TruncatedRepeatTransactionDays DOUBLE,
TruncatedRepeatAverageSpend DOUBLE
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
LOCATION '/user/${system:user.name}/queryresult/${hiveconf:hivedb}/final/data/';
