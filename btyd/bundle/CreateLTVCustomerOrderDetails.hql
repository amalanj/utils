USE ${hiveconf:hivedb};
DROP TABLE IF EXISTS ActiveCustomers;

CREATE TABLE ActiveCustomers AS SELECT
customers_id as CustomerKey,
date_account_created as AcquisitionDate,
(
CASE
WHEN customers_id > 0 THEN DATE_ADD(from_unixtime(unix_timestamp()), - 365)
ELSE NULL
END
) AS CalibStartDate,
(
CASE WHEN customers_id > 0 THEN DATE_ADD(from_unixtime(unix_timestamp()), - 1) ELSE NULL END
) AS CalibEndDate
FROM
customers
WHERE customers_status = 1;


DROP TABLE IF EXISTS CustomerPurchaseDetails;

CREATE TABLE CustomerPurchaseDetails AS SELECT
orders.customers_id as CustomerKey,
orders.orders_id as OrderNum,
orders.orders_status as OrderStatus,
orders.invoice_date as DemandDate,
orders.orders_date_finished as ShipDate,
ac.AcquisitionDate,
ac.CalibStartDate,
ac.CalibEndDate,
COALESCE(orders_products.products_price, 0) AS GrossSalesAmt,
0 AS DiscountAmt,
COALESCE(orders_products.products_quantity, 0) AS ItemQty,
orders.orders_id AS UniqueTransactionNum,
orders.date_purchased AS PurchaseDate
FROM
orders orders, orders_products orders_products
INNER JOIN
ActiveCustomers ac
ON 
orders.customers_id = ac.CustomerKey
WHERE
orders.orders_id = orders_products.orders_id;


DROP TABLE IF EXISTS CustomerFirstPurchase;

CREATE TABLE CustomerFirstPurchase AS SELECT
CustomerKey, 
MIN(PurchaseDate) AS FirstPurchaseDate
FROM 
CustomerPurchaseDetails
GROUP BY CustomerKey;


DROP TABLE IF EXISTS CustomerPurchase;

CREATE TABLE CustomerPurchase
AS SELECT
cpdt.*, 
cfpt.FirstPurchaseDate
FROM 
CustomerPurchaseDetails cpdt
LEFT OUTER JOIN
CustomerFirstPurchase cfpt
ON 
cpdt.CustomerKey = cfpt.CustomerKey;



DROP TABLE IF EXISTS CustomerOrderDetails;

CREATE TABLE CustomerOrderDetails
AS SELECT
CustomerKey,
AcquisitionDate,
CalibstartDate,
CalibendDate,
UniqueTransactionNum as UniqueTransactionNumber,
GrosssalesAmt as GrossSalesAmount,
DiscountAmt as DiscountAmount,
ItemQty as ItemQuantity,
PurchaseDate,
(
CASE
WHEN cp.PurchaseDate = cp.FirstPurchaseDate THEN CAST(0 AS FLOAT)
ELSE cp.GrossSalesAmt
END
) AS LTVSalesAmount,
(
CASE
WHEN cp.PurchaseDate = cp.FirstPurchaseDate THEN CAST(0 AS FLOAT)
ELSE cp.DiscountAmt
END
) AS LTVDiscountAmount,
(
CASE
WHEN cp.PurchaseDate = cp.FirstPurchaseDate THEN 0L
ELSE cp.ItemQty
END
) AS LTVItemQuantity,
from_unixtime(unix_timestamp()) as RunDate
FROM 
CustomerPurchase cp;
