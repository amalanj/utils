USE ${hiveconf:hivedb};
DROP TABLE IF EXISTS TransactionDetails;

CREATE TABLE TransactionDetails
AS SELECT 
CustomerKey,
MIN(AcquisitionDate) AS AcquisitionDate,
MIN(CalibStartDate) AS CalibStartDate,
MAX(CalibEndDate) AS CalibEndDate,
MIN(PurchaseDate) AS MinPurchaseDate, 
MAX(PurchaseDate) AS MaxPurchaseDate, 
COUNT(DISTINCT PurchaseDate) AS TransactionDays, 
COUNT(DISTINCT PurchaseDate) - 1 AS RepeatTransactionDays,
COUNT(DISTINCT UniqueTransactionNumber) AS Transactions,
COUNT(DISTINCT UniqueTransactionNumber) - 1 AS RepeatTransactions,
SUM(GrossSalesAmount) + SUM(DiscountAmount) AS Spend,
SUM(LTVSalesAmount) + SUM(LTVDiscountAmount) AS RepeatSpend 
FROM 
CustomerOrderDetails 
GROUP BY CustomerKey;



DROP TABLE IF EXISTS CustomerAggregates;

CREATE TABLE CustomerAggregates
AS SELECT 
CustomerKey,
AcquisitionDate,
CalibStartDate,
CalibEndDate,
MinPurchaseDate,
MaxPurchaseDate,
TransactionDays,
Spend,
RepeatTransactionDays,
RepeatSpend,
(Spend / TransactionDays) AS AverageSpend,
(
CASE 
WHEN RepeatTransactionDays > 0 THEN ( RepeatSpend / RepeatTransactionDays )
ELSE CAST(0 AS DOUBLE)
END
) AS RepeatAverageSpend, 
DATEDIFF(MaxPurchaseDate, MinPurchaseDate) / 7 AS CustomerDuration,
DATEDIFF(CalibEndDate, MinPurchaseDate) / 7 AS StudyDuration,
from_unixtime(unix_timestamp()) as RunDate
FROM 
TransactionDetails ;
