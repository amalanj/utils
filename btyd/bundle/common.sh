
set -e

function print_debug {
  printf '%s DEBUG' $(date +%FT%T) >&2
  printf ' %s' "$@" >&2
  printf '\n' >&2 
}

function print_info {
  printf '%s INFO' $(date +%FT%T) >&2
  printf ' %s' "$@" >&2
  printf '\n' >&2
}

function print_warn {
  printf '%s WARN ' $(date +%FT%T) >&2
  printf ' %s' "$@" >&2
  printf '\n' >&2
}

function print_error {
  printf '%s ERROR ' $(date +%FT%T) >&2
  printf ' %s' "$@" >&2
  printf '\n' >&2
}