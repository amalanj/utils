// ORM class for table 'null'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Thu Mar 10 19:34:41 IST 2016
// For connector: org.apache.sqoop.manager.MySQLManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class QueryResult extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private Integer customers_id;
  public Integer get_customers_id() {
    return customers_id;
  }
  public void set_customers_id(Integer customers_id) {
    this.customers_id = customers_id;
  }
  public QueryResult with_customers_id(Integer customers_id) {
    this.customers_id = customers_id;
    return this;
  }
  private Integer customers_status;
  public Integer get_customers_status() {
    return customers_status;
  }
  public void set_customers_status(Integer customers_status) {
    this.customers_status = customers_status;
  }
  public QueryResult with_customers_status(Integer customers_status) {
    this.customers_status = customers_status;
    return this;
  }
  private String date_format_date_account_created___y__m__d__;
  public String get_date_format_date_account_created___y__m__d__() {
    return date_format_date_account_created___y__m__d__;
  }
  public void set_date_format_date_account_created___y__m__d__(String date_format_date_account_created___y__m__d__) {
    this.date_format_date_account_created___y__m__d__ = date_format_date_account_created___y__m__d__;
  }
  public QueryResult with_date_format_date_account_created___y__m__d__(String date_format_date_account_created___y__m__d__) {
    this.date_format_date_account_created___y__m__d__ = date_format_date_account_created___y__m__d__;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.customers_id == null ? that.customers_id == null : this.customers_id.equals(that.customers_id));
    equal = equal && (this.customers_status == null ? that.customers_status == null : this.customers_status.equals(that.customers_status));
    equal = equal && (this.date_format_date_account_created___y__m__d__ == null ? that.date_format_date_account_created___y__m__d__ == null : this.date_format_date_account_created___y__m__d__.equals(that.date_format_date_account_created___y__m__d__));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.customers_id = JdbcWritableBridge.readInteger(1, __dbResults);
    this.customers_status = JdbcWritableBridge.readInteger(2, __dbResults);
    this.date_format_date_account_created___y__m__d__ = JdbcWritableBridge.readString(3, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeInteger(customers_id, 1 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(customers_status, 2 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(date_format_date_account_created___y__m__d__, 3 + __off, 12, __dbStmt);
    return 3;
  }
  public void readFields(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.customers_id = null;
    } else {
    this.customers_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.customers_status = null;
    } else {
    this.customers_status = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.date_format_date_account_created___y__m__d__ = null;
    } else {
    this.date_format_date_account_created___y__m__d__ = Text.readString(__dataIn);
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.customers_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.customers_id);
    }
    if (null == this.customers_status) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.customers_status);
    }
    if (null == this.date_format_date_account_created___y__m__d__) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, date_format_date_account_created___y__m__d__);
    }
  }
  private final DelimiterSet __outputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(customers_id==null?"null":"" + customers_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(customers_status==null?"null":"" + customers_status, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(date_format_date_account_created___y__m__d__==null?"null":date_format_date_account_created___y__m__d__, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  private final DelimiterSet __inputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str;
    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.customers_id = null; } else {
      this.customers_id = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.customers_status = null; } else {
      this.customers_status = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.date_format_date_account_created___y__m__d__ = null; } else {
      this.date_format_date_account_created___y__m__d__ = __cur_str;
    }

  }

  public Object clone() throws CloneNotSupportedException {
    QueryResult o = (QueryResult) super.clone();
    return o;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("customers_id", this.customers_id);
    __sqoop$field_map.put("customers_status", this.customers_status);
    __sqoop$field_map.put("date_format_date_account_created___y__m__d__", this.date_format_date_account_created___y__m__d__);
    return __sqoop$field_map;
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("customers_id".equals(__fieldName)) {
      this.customers_id = (Integer) __fieldVal;
    }
    else    if ("customers_status".equals(__fieldName)) {
      this.customers_status = (Integer) __fieldVal;
    }
    else    if ("date_format_date_account_created___y__m__d__".equals(__fieldName)) {
      this.date_format_date_account_created___y__m__d__ = (String) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
}
