USE ${hiveconf:hivedb};
DROP TABLE scoringTableInit;
CREATE EXTERNAL TABLE scoringTableInit
(
    CustomerKey                 BIGINT,
    AquisitionDate              STRING,
    CalibStartDate              STRING,
    CalibEndDate                STRING,
    ForecastedTransactions12    DOUBLE,
    ForecastedAverageSpends12   DOUBLE,
    ExpectedLTV12               DOUBLE,
    ForecastedTransactions18    DOUBLE,
    ForecastedAverageSpends18   DOUBLE,
    ExpectedLTV18               DOUBLE
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION '/user/${system:user.name}/queryresult/init';

DROP TABLE scoringTableInc;
CREATE EXTERNAL TABLE scoringTableInc
(
    CustomerKey                 BIGINT,
    AquisitionDate              STRING,
    CalibStartDate              STRING,
    CalibEndDate                STRING,
    ForecastedTransactions12    DOUBLE,
    ForecastedAverageSpends12   DOUBLE,
    ExpectedLTV12            	DOUBLE,
    ForecastedTransactions18    DOUBLE,
    ForecastedAverageSpends18   DOUBLE,
    ExpectedLTV18            	DOUBLE
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION '/user/${system:user.name}/queryresult/${hiveconf:hivedb}/final/score';

DROP TABLE scoringTable;
CREATE TABLE scoringTable AS 
 SELECT * FROM (
    SELECT * FROM scoringTableInit
    UNION ALL
    SELECT * FROM scoringTableInc) finalScore;


