#!/usr/bin/env bash

echo "$#"
if [ "$#" -ne 4 ]; then
    echo "Try running the application with following MySQL parameters. Hostname, Database name, Username, Password"
    echo "bundle/run.sh hostname databasename, username, password"
    exit
fi

HOST=$1
DATABASE=$2
USERNAME=$3
PASSWORD=$4
CURRENTDIR="$(cd "$(dirname "$0")" && pwd)"
CURRUSER=$USER
HDFSPATH=/user/$CURRUSER/queryresult/$DATABASE

ONETIME=false
DODUMP=false
DOIMPORT=false
DOEXTTABLESCREATE=false
DOCALLHQL=true
DODATAPREP=true
DOCALLR=true

echo "Source bashrc and common.sh"
source ~/.bashrc
source $CURRENTDIR/common.sh

print_info Host is $HOST
print_info Database is $DATABASE
print_info Username is $USERNAME
print_info Password is $PASSWORD
print_info Current Dir is $CURRENTDIR
print_info Current user is $CURRUSER
print_info HDFS path is $HDFSPATH

print_info START

if $ONETIME; then
  if $($HADOOP_HOME/bin/hadoop fs -test -d queryresult/training) ; then 
    print_info queryresult/training exists 
  else 
    $HADOOP_HOME/bin/hadoop dfs -mkdir queryresult/training
    $HADOOP_HOME/bin/hadoop dfs -copyFromLocal $CURRENTDIR/scoring/training/* queryresult/training/;
  fi
  if $($HADOOP_HOME/bin/hadoop fs -test -d queryresult/init) ; then 
    print_info queryresult/init exists
  else 
    $HADOOP_HOME/bin/hadoop dfs -mkdir queryresult/init
    $HADOOP_HOME/bin/hadoop dfs -copyFromLocal $CURRENTDIR/scoring/initial_scoring.txt queryresult/init/;
  fi
fi

if $DODUMP; then

  print_info Create the database if not exist in MySQL and drop main tables to reload
  mysql -u $USERNAME -p$PASSWORD -h $HOST -e "CREATE DATABASE IF NOT EXISTS $DATABASE"
  mysql -u $USERNAME -p$PASSWORD -h $HOST -e "DROP TABLE IF EXISTS $DATABASE.byfx_customers"
  mysql -u $USERNAME -p$PASSWORD -h $HOST -e "DROP TABLE IF EXISTS $DATABASE.byfx_orders"
  mysql -u $USERNAME -p$PASSWORD -h $HOST -e "DROP TABLE IF EXISTS $DATABASE.byfx_orders_products"

  print_info Dumping MySQL data into "$DATABASE"
  gunzip < $CURRENTDIR/demodump.sql.gz  | mysql -u $USERNAME -p$PASSWORD $DATABASE 

fi

if $DOIMPORT; then

  print_warn Remove HDFS directory if exists "$HDFSPATH"
  if $($HADOOP_HOME/bin/hadoop fs -test -d $HDFSPATH) ; then 
    $HADOOP_HOME/bin/hadoop dfs -rmr $HDFSPATH;
  fi

  CUST_QUERY="SELECT customers_id, customers_status, date_format(date_account_created,'%Y-%m-%d') FROM $DATABASE.byfx_customers WHERE \$CONDITIONS"
  ORD_QUERY="SELECT orders_id, customers_id, orders_status, date_format(invoice_date,'%Y-%m-%d'), date_format(orders_date_finished,'%Y-%m-%d'), date_format(date_purchased,'%Y-%m-%d') FROM $DATABASE.byfx_orders WHERE \$CONDITIONS"
  ORD_PROD_QUERY="SELECT orders_id, products_price, products_quantity FROM $DATABASE.byfx_orders_products WHERE \$CONDITIONS"

  print_info Run SQOOP import on Customers
  sqoop import --connect jdbc:mysql://$HOST/$DATABASE \
    --username $USERNAME -password $PASSWORD \
    --query "$CUST_QUERY" \
    --m 1 \
    --target-dir $HDFSPATH/customers
  print_debug SQOOP import on Customers returns "echo $?"

  print_info Run SQOOP import on Orders
  sqoop import --connect jdbc:mysql://$HOST/$DATABASE \
    --username $USERNAME -password $PASSWORD \
    --query "$ORD_QUERY" \
    --m 1 \
    --target-dir $HDFSPATH/orders
  print_debug SQOOP import on Orders returns "echo $?"

  print_info Run SQOOP import on Orders_Products
  sqoop import --connect jdbc:mysql://$HOST/$DATABASE \
    --username $USERNAME -password $PASSWORD \
    --query "$ORD_PROD_QUERY" \
    --m 1 \
    --target-dir $HDFSPATH/orders_products
  print_debug SQOOP import on Orders_Products returns "echo $?"

fi

if $DOEXTTABLESCREATE; then

  #$HIVE_HOME/bin/hive -e "DROP TABLE IF EXISTS "$DATABASE".customers;"
  #$HIVE_HOME/bin/hive -e "DROP TABLE IF EXISTS "$DATABASE".orders;"
  #$HIVE_HOME/bin/hive -e "DROP TABLE IF EXISTS "$DATABASE".orders_products;"
  print_warn Drop the database "$DATABASE" if exists cascade
  $HIVE_HOME/bin/hive -S -e "DROP DATABASE IF EXISTS "$DATABASE" CASCADE;"
  print_info Create database "$DATABASE"
  $HIVE_HOME/bin/hive -S -e "CREATE DATABASE "$DATABASE";"
  print_info Use the database "$DATABASE"
  $HIVE_HOME/bin/hive -S -e "USE "$DATABASE";"

  print_info Create external table for customers on database "$DATABASE"
  $HIVE_HOME/bin/hive -S -e "CREATE EXTERNAL TABLE "$DATABASE".customers(customers_id INT, customers_status STRING, date_account_created DATE) ROW FORMAT
                DELIMITED FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n' 
                STORED AS TEXTFILE
                LOCATION '"$HDFSPATH"/customers';"

  print_info Create external table for orders on database "$DATABASE"
  $HIVE_HOME/bin/hive -S -e "CREATE EXTERNAL TABLE "$DATABASE".orders(orders_id INT, customers_id INT, orders_status STRING, invoice_date DATE, orders_date_finished DATE, date_purchased DATE) ROW FORMAT
                DELIMITED FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n' 
                STORED AS TEXTFILE
                LOCATION '"$HDFSPATH"/orders';"

  print_info Create external table for orders_products on database "$DATABASE"
  $HIVE_HOME/bin/hive -S -e "CREATE EXTERNAL TABLE "$DATABASE".orders_products(orders_id INT, products_price FLOAT, products_quantity INT) ROW FORMAT
                DELIMITED FIELDS TERMINATED BY ','
                LINES TERMINATED BY '\n' 
                STORED AS TEXTFILE
                LOCATION '"$HDFSPATH"/orders_products';"

fi

if $DOCALLHQL; then

  print_info Run CreateLTVCustomerOrderDetails.hql
  $HIVE_HOME/bin/hive -hiveconf hivedb=$DATABASE -f $CURRENTDIR/CreateLTVCustomerOrderDetails.hql

  print_info Run CreateLTVCustomerAggregates.hql
  $HIVE_HOME/bin/hive -hiveconf hivedb=$DATABASE -f $CURRENTDIR/CreateLTVCustomerAggregates.hql

  print_info Run CreateLTVCustomerAggregatesTruncated.hql
  $HIVE_HOME/bin/hive -hiveconf hivedb=$DATABASE -f $CURRENTDIR/CreateLTVCustomerAggregatesTruncated.hql

  print_info Run CreateLifeTimeValueDataprepFinal.hql
  $HIVE_HOME/bin/hive -hiveconf hivedb=$DATABASE -f $CURRENTDIR/CreateLifeTimeValueDataprepFinal.hql

  print_info Run CreateLifeTimeValue.hql
  $HIVE_HOME/bin/hive -hiveconf hivedb=$DATABASE hiveuser=$CURRUSER -f $CURRENTDIR/CreateLifeTimeValue.hql

fi

if $DODATAPREP; then

  print_info Export rows from CustomerAggregatesTruncated to local directory
  $HIVE_HOME/bin/hive -S -e "INSERT OVERWRITE LOCAL DIRECTORY '$CURRENTDIR/CustomerData'
  ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY ',' 
  SELECT CustomerKey, AcquisitionDate, CalibStartDate, CalibEndDate, MinPurchaseDate, 
    MaxPurchaseDate, TransactionDays, Spend, RepeatTransactionDays, RepeatSpend, CustomerDuration, 
    StudyDuration, TruncatedTransactionDays, TruncatedAverageSpend, TruncatedRepeatTransactionDays, 
    TruncatedRepeatAverageSpend FROM "$DATABASE".CustomerAggregatesTruncated;"

  $HADOOP_HOME/bin/hadoop dfs -rmr $HDFSPATH/final/*
  $HADOOP_HOME/bin/hadoop dfs -mkdir $HDFSPATH/final/data
  print_info Copy the exported data back to hdfs location $HDFSPATH/final/data
  $HADOOP_HOME/bin/hadoop dfs -copyFromLocal $CURRENTDIR/CustomerData/000000_0 $HDFSPATH/final/data

fi

if $DOCALLR; then

  print_info Time to invoke the R script to compute the LTV
  $HADOOP_HOME/bin/Rscript $CURRENTDIR/scoring/ComputeLifeTimeValue.R $DATABASE

fi

print_info END



