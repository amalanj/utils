USE ${hiveconf:hivedb};
DROP TABLE IF EXISTS CustomerTransactionsPercentile;

CREATE TABLE CustomerTransactionsPercentile
AS SELECT 
ca.*,
cpercentiles.TransactionDays_99_75, 
cpercentiles.AverageSpend_99_75,
cpercentiles.RepeatTransactionDays_99_75, 
cpercentiles.RepeatAverageSpend_99_75
FROM
CustomerAggregates ca
INNER JOIN
(
SELECT
PERCENTILE(TransactionDays, 0.9975) AS TransactionDays_99_75,
PERCENTILE_APPROX(AverageSpend, 0.9975) AS AverageSpend_99_75,
PERCENTILE(RepeatTransactionDays, 0.9975) AS RepeatTransactionDays_99_75,
PERCENTILE_APPROX(RepeatAverageSpend, 0.9975) AS RepeatAverageSpend_99_75
FROM
CustomerAggregates
) cpercentiles;



DROP TABLE IF EXISTS CustomerTruncatedSpendsAndTransactions;

CREATE TABLE CustomerTruncatedSpendsAndTransactions
AS SELECT 
*,
(
CASE 
WHEN AverageSpend > AverageSpend_99_75 THEN AverageSpend_99_75
WHEN AverageSpend < 0 THEN CAST(0 AS DOUBLE)
ELSE AverageSpend
END
) AS TruncatedAverageSpend,
(
CASE 
WHEN TransactionDays > TransactionDays_99_75 THEN TransactionDays_99_75
ELSE CAST(TransactionDays AS DOUBLE)
END
) AS TruncatedTransactionDays,
(
CASE 
WHEN RepeatAverageSpend > RepeatAverageSpend_99_75 THEN RepeatAverageSpend_99_75
WHEN RepeatAverageSpend < 0 THEN CAST(0 AS DOUBLE)
ELSE RepeatAverageSpend
END
) AS TruncatedRepeatAverageSpend,
(
CASE 
WHEN RepeatTransactionDays > RepeatTransactionDays_99_75 THEN RepeatTransactionDays_99_75
ELSE CAST(RepeatTransactionDays AS DOUBLE)
END
) AS TruncatedRepeatTransactionDays
FROM
CustomerTransactionsPercentile ;


DROP TABLE IF EXISTS CustomerAggregatesTruncated;

CREATE TABLE CustomerAggregatesTruncated
AS SELECT
CAST(CustomerKey AS BIGINT),
CAST(AcquisitionDate AS STRING),
CAST(CalibStartDate AS STRING),
CAST(CalibEndDate AS STRING),
CAST(MinPurchaseDate AS STRING),
CAST(MaxPurchaseDate AS STRING),
CAST(TransactionDays AS DOUBLE),
CAST(Spend AS DOUBLE),
CAST(RepeatTransactionDays AS DOUBLE),
CAST(RepeatSpend AS DOUBLE),
CAST(CustomerDuration AS DOUBLE),
CAST(StudyDuration AS DOUBLE),
CAST(TruncatedTransactionDays AS DOUBLE),
CAST(TruncatedAverageSpend AS DOUBLE),
CAST(TruncatedRepeatTransactionDays AS DOUBLE),
CAST(TruncatedRepeatAverageSpend AS DOUBLE),
from_unixtime(unix_timestamp()) as RunDate
FROM CustomerTruncatedSpendsAndTransactions;
