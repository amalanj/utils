#!/usr/bin/env bash

source ~/.bashrc 
HASPASSED=true

#CURRENTDIR="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"

#ADHOC_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

CURRENTDIR="$(cd "$(dirname "$0")" && pwd)"
myvariable=$USER
echo $myvariable
echo $CURRENTDIR

if $HASPASSED; then
	echo "passed is $HASPASSED"
fi

HDFSPATH="/user/amalan/queryresult/demo3"

if $($HADOOP_HOME/bin/hadoop fs -test -d $HDFSPATH) ; then echo "ok";else echo "not ok"; fi