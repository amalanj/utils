#!/usr/bin/Rscript

## Collect arguments
args <- commandArgs(trailingOnly = FALSE)
file.arg.name <- "--file="
script.name <- sub(file.arg.name, "", args[grep(file.arg.name, args)])
script.basename <- dirname(script.name)
ArgumentParser.name <- paste(sep="/", script.basename, "ArgumentParser.R")
EnvironmentSetup.name <- paste(sep="/", script.basename, "EnvironmentSetup.R")
CommonFunctions.name <- paste(sep="/", script.basename, "CommonFunctions.R")
source(ArgumentParser.name)
source(EnvironmentSetup.name)
source(CommonFunctions.name)

printLog(paste("Argument value :", args[6]))

db <- trim(args[6])
printLog(paste("DB :", db))
#Compute brand-date Paths
#kCustomerDataPath <- paste(kCustomerDataPath, kBrandName, sep = "/")
#kOutputPath <- paste(kOutputPath, kBrandName, kRunDate, sep = "/")

#kCustomerDataPath <- "/queryresult/final/"
kCustomerDataPath <- paste("/user/",Sys.getenv("USER"),"/queryresult/",db,"/final/data/000000_0",sep="")
kOutputPath <- paste("/user/",Sys.getenv("USER"),"/queryresult/",db,"/final/score/",sep="")
kSpendsTrainingDataPath <- paste("/user/",Sys.getenv("USER"),"/queryresult/training/spends.csv",sep="")
kTransactionsTrainingDataPath <- paste("/user/",Sys.getenv("USER"),"/queryresult/training/transactions.csv",sep="")
kForecastingPeriod12 <- 52
kForecastingPeriod18 <- 78
kRunInMapReduce <- TRUE
#Capture paths
printLog(paste("Dataprep path :", kCustomerDataPath))
printLog(paste("Training data for average spends path :", kSpendsTrainingDataPath))
printLog(paste("Training data for transactions path :", kTransactionsTrainingDataPath))
printLog(paste("Scoring output path :", kOutputPath))
printLog(paste("Forecasting period for 12 months in weeks :", kForecastingPeriod12))
printLog(paste("Forecasting period for 18 months in weeks :", kForecastingPeriod18))
#printLog(paste("Brand name :", kBrandName))
printLog(paste("Whether to run in MapReduce mode :", kRunInMapReduce))

#Function to compute LTV columns
ComputeLTV <- function (values) {
  values <- na.omit(values)
  
  forecasted.transactions.12 <- ForecastTransactions(training.data.transactions, kForecastingPeriod12, values$TruncatedRepeatTransactionDays, values$CustomerDuration, values$StudyDuration)
  forecasted.average.spends <- ForecastAverageSpend(training.data.spends, values$TruncatedRepeatAverageSpend, values$TruncatedRepeatTransactionDays)
  expected.ltv.12 <- ComputeExpectedLTV(forecasted.transactions.12, forecasted.average.spends)    
  
  forecasted.transactions.18 <- ForecastTransactions(training.data.transactions, kForecastingPeriod18, values$TruncatedRepeatTransactionDays, values$CustomerDuration, values$StudyDuration)    
  expected.ltv.18 <- ComputeExpectedLTV(forecasted.transactions.18, forecasted.average.spends)
  
  output.value <- data.frame(CustomerKey = values$CustomerKey, 
                             AquisitionDate = values$AquisitionDate,
                             CalibStartDate = values$CalibStartDate,
                             CalibEndDate = values$CalibEndDate,
                             ForecastedTransactions12 = forecasted.transactions.12,
                             ForecastedAverageSpends12 = forecasted.average.spends,
                             ExpectedLTV12 = expected.ltv.12,
                             ForecastedTransactions18 = forecasted.transactions.18,
                             ForecastedAverageSpends18 = forecasted.average.spends,
                             ExpectedLTV18 = expected.ltv.18,
                             stringsAsFactors = FALSE)
}                            

#Function to compute LTV in MapReduce
ComputeLTVInMR <- function (inputPath, inputFormat, outputPath = NULL) {  
  mapper <- function(key, values) {   
    output.key <- NULL    
    output.value <- ComputeLTV(values)    
    keyval(output.key, output.value)
  }
  
  mapreduce (
    input = inputPath,
    input.format = inputFormat,
    map = mapper,
    output = outputPath,
    output.format = make.output.format("csv", sep = ",", quote = FALSE),
    verbose=T
  )
}

#Function to compute LTV in Memory
ComputeLTVInMemory <- function(inputPath, inputFormat, outputPath = NULL) {
  values <- LoadFromDfs(inputPath, inputFormat)    
  output.value <- ComputeLTV(values)    
  WriteToDfs(output.value, outputPath)
}

#Load training datasets
printLog("Loading training data for customer transactions")
training.data.transactions <- LoadCsvFromDfsToVector(kTransactionsTrainingDataPath)

printLog("Loading training data for customer average spends")
training.data.spends <- LoadCsvFromDfsToVector(kSpendsTrainingDataPath)

#Compute LTV
customer.data.path <- kCustomerDataPath
customer.data.columns <- c('CustomerKey', 'AquisitionDate', 'CalibStartDate', 'CalibEndDate', 'MinimumPurchaseDate', 'MaximumPurchaseDate',
                                  'TransactionDays', 'Spend', 'RepeatTransactionDays', 'RepeatSpend', 'CustomerDuration', 'StudyDuration', 
                                  'TruncatedTransactionDays', 'TruncatedAverageSpend', 'TruncatedRepeatTransactionDays', 'TruncatedRepeatAverageSpend')
customer.data.format <- make.input.format('csv', 'text', sep=',', col.names = customer.data.columns, stringsAsFactors=FALSE, na.strings="\\N")

RemoveDfsPath(kOutputPath)
if(kRunInMapReduce) {
  printLog(paste("Computing LTV in MapReduce mode", ""))
  ComputeLTVInMR(customer.data.path, customer.data.format, kOutputPath)
} else {
  printLog(paste("Computing LTV in standalone (in memory) mode", ""))
  ComputeLTVInMemory(customer.data.path, customer.data.format, kOutputPath)
}
printLog(paste("LTV Scoring completed successfully. Output is available at ", kOutputPath))

