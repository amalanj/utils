# function to read and prepare arguments list as key value pairs
CommandLineArguments <- function() {
    return(commandArgs(TRUE))
}

GetProperties <- function(clArgs) {
    properties <- list()
    counter <- 1;
    for (arg in clArgs) {
        if (substr(arg, 1, 2) == '--' )  {
            arg <- substring(arg, 3);
            kv <- strsplit(arg, '=')[[1]]
            if (!is.na(kv[2])) {
                properties[[counter]] <- cbind(kv[1], kv[2])
                counter <- counter + 1;
            }
        }
        next;
    }
    return(properties)
}

AsNumericOrCharacter <- function(value) {
    if(is.na(as.numeric(value))) {
        return(value)
    } else {
        return(as.numeric(value))
    }
}

AssignGlobalConstants <- function(properties) {
    lapply(
        properties, function(prop) {
            assign(as.character(prop[[1]]), AsNumericOrCharacter(prop[[2]]), envir = .GlobalEnv)
        }
    )
}

# Execution
AssignGlobalConstants(GetProperties(CommandLineArguments()))
